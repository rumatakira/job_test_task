# Kiryl A.Lapchynski job application test task. HHVM(3.18-lts), MySQL(5.7), Yii(2) based
## **Installation:**
1. Clone this repo in any new dir you want. Open terminal in the child directory job_test_task and run (! NOTE: ~900M will be downloaded):

	$ docker-compose up -d
	
	$ docker-compose exec jtt-hhvm hhvm yii migrate --interactive=false

2. Application now is available at http://localhost:7777

3. Code available at this repository - [here](https://bitbucket.org/rumatakira/job_test_task)</a>

4. Populate database - from main menu [News test]->[Database population (Faker)]

5. To stop application just run: $docker-compose down

6. To fully remove application from you system additonally run:
	
	$ docker rmi rumatakira/job_test_task rumatakira/mysql-server
	
	$ docker volume rm jobtesttask_db_data
	
	And remove the application directory

_______________________________________________________________

## **Установка :**

1. Клонируйте этот репозиторий в любую новую директорию на вашем компьютере. Откройте терминал из внутренней под-директории job_test_task и выполните (! Примерно 900М будет загружено из интернета):

	$ docker-compose up -d
	
	$ docker-compose exec jtt-hhvm hhvm yii migrate --interactive=false
	
2. Теперь приложение доступно по адресу: http://localhost:7777
3. Базовый код доступен [тут](https://bitbucket.org/rumatakira/job_test_task)
4. Заполните базу данными из главного меню - [News test]->[Database population (Faker)].
5. Чтобы остановить приложение выполните из ранее открытого терминала: 

	$docker-compose down
	
6. Для полного удаления приложения из вашей системы выполните:
	
	$ docker rmi rumatakira/job_test_task rumatakira/mysql-server
	
	$ docker volume rm jobtesttask_db_data
	
	И удалите саму директорию, которую вы создали в начале