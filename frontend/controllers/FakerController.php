<?php

/**
 * Truncate tables {{news}}, and populates
 * {{news}} with 20 fakes newss.
 */

namespace frontend\controllers;

use Yii;
use Faker;
use yii\web\Controller;
use app\modules\admin\models\NewsForm;


define('__VENDOR__', dirname(dirname(__DIR__)));
require_once __VENDOR__.'/vendor/fzaninotto/faker/src/autoload.php';

class FakerController extends Controller
{

    public function actionFaker()
    {
        $faker = Faker\Factory::create('en_EN');
        $faker->seed(1234);// uncomment for the random fake data
        Yii::$app->db->createCommand('TRUNCATE TABLE news')->execute();
        
        for ($i = 1; $i < 4; ++$i) {
            $news = new NewsForm();
            $news->news_title = $faker->realText($maxNbChars = 50, $indexSize = 2);
            $news->news_created_at = "2016-5-$i";
            $news->news_body = $faker->realText($maxNbChars = 1200, $indexSize = 2);
            $news->news_topic = $faker->randomElement($array = array('Russia', 'Belarus', 'Ukraine'));
            $news->save();
        }
        
        for ($i = 1; $i < 18; ++$i) {
            $news                  = new NewsForm();
            $news->news_title      = $faker->realText($maxNbChars = 50, $indexSize = 2);
            $news->news_created_at = $faker->date($format = 'Y-m-d', $max = 'now');
            $news->news_body       = $faker->realText($maxNbChars = 1200, $indexSize = 2);
            $news->news_topic      = $faker->randomElement($array = array('Russia', 'Belarus', 'Ukraine'));
            
            if ($news->save()) {
                Yii::$app->session->setFlash('success', 'Database populated.');
            } else {
                Yii::$app->session->setFlash('error', 'Oops... Something went wrong.');
            }
        }

        return $this->goHome();

    }//end actionFaker()


}//end class