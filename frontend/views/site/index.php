<div class="site-index"  text-align: left>
    <a name="TOP"></a>
    <a href="#EN">EN</a>
    <a href="#RU">RU</a>
    <a name="EN"></a>
    </br><hr>
    <p><big><i>Kiryl A.Lapchynski job application test task. HHVM(3.18-lts), MySQL(5.7), Yii(2) based.</i></big></p>
    
    <p><ul><b>Installation:</b>
        <li>Clone this repo in any new dir you want. Open terminal in the child directory job_test_task and run:</li>
        <p>$ docker-compose up -d</p>
        <p>$ docker-compose exec jtt-hhvm hhvm yii migrate --interactive=false</p>
        <li>Application now is available at http://localhost:7777</li>
        <li>Code available at this <a href="https://bitbucket.org/rumatakira/job_test_task">repository</a></li>
        <li>Populate database - from main menu [News test]->[Database population (Faker)].</li>
        <li>To stop application just run: $docker-compose down
        <li>To fully remove application from you system additionally run:</li>
        <p>$ docker rmi rumatakira/job_test_task rumatakira/mysql-server</p>
        <p>$ docker volume rm jobtesttask_db_data</p>
        <li>And remove the application directory</li>   
    </ul>
    <p><ul><b>Tasks :</b>
        <li>Relize this task using Yii2 framework.</li>
        <li>Write a news editing and output module.</li>
        <li>Module to create and edit news (admin module): Output of all the news, sorted by date (the last top). Creating and editing news. Every news has fields such as title, date of publication, text, the topic (the news is bound to a single topic from the list) the date relize using jquery calendar to edit the field (http://jqueryui.com/).</li>
    </ul>
    </br><u>News output module:</u>
    </br>At the left screen sude - show all years for which there are publications, for each year show the months for which there are publications, in parentheses indicate the number of publications for that month, as:
    <ul>
        <li>2010</li>
        <li>September (1)</li>
        <li>July (4)</li>
        <li>June (7)</li>
        <li>March (12)</li>
        <li>February (3)</li>
        <li>2009</li>
    </ul>
    </br>By clicking on the year - show all news of this year, when clicked the month - show all news of this month. Under the years in parentheses the number of publications on the topic, as:
    <ul>
        <li>theme1 (2)
        <li>theme2 (5)
        <li>theme3 (7)</li>
    </ul>
    </br>When you click on the topic - show all the news for this topic. The right screen side - show all news as:
    <ul>
        <li>Name</li>
        <li>Date of publication</li>
        <li>Subject</li>
        <li>Short text (full text trimmed to 256 characters) ... read more (link to news)</li>
    </ul>
    </br>when you click on "read more" - news shows in the form of:
    <ul>
        <li>Name</li>
        <li>Publication date</li>
        <li>Subject</li>
        <li>The text of the news completely</li>
        <li>more news (link to the page with the news)</li>
    </ul>
    </br>The output of news on page 5, at the bottom of the page a pager (output all pages, current page is highlighted). Glamorous design is not required - important to implement this task. </br>
    </br><u>SQL Task:</u>
    </br>Convert the data table so that it contained only transactions-expenses. All refunds should be considered in their previous transaction-costs. Example. Dump data contains the following two transactions.</br>
    <table style="width:100%">
        <tr>
            <th>ID</th>
            <th>Card number</th>
            <th>Date/time</th>
            <th>Volume</th>
            <th>Service</th>
        </tr>
        <tr>
            <th>31769</th>
            <th>257473011</th>
            <th>2015-10-18 11:10:36</th>
            <th>-68</th>
            <th>DT</th>
        </tr>
        <tr>
            <th>31768</th>
            <th>257473011</th>
            <th>2015-10-18 11:21:15</th>
            <th>2.44</th>
            <th>DT</th>
        </tr>
    </table>
    </br>Where are transaction ID 31769 - consumption and ID 31768 - return on this transaction. As a result of the conversion, the transaction ID 31769 should have a volume of -65.56, and the transaction ID 31768 must be removed.
Dump .sql attached.
    </br><hr>
    <a name="RU">На русском:</a>
    <p><ul><b>Установка:</b> 
        <li>Клонируйте этот репозиторий в любую новую директорию на вашем компьютере. Откройте терминал из внутренней под-директории job_test_task и выполните:</li>
        <p>$ docker-compose up -d</p>
        <p>$ docker-compose exec jtt-hhvm hhvm yii migrate --interactive=false</p>
        <li>Теперь приложение доступно по адресу: http://localhost:7777</li>
        <li>Базовый код доступен в этом <a href="https://bitbucket.org/rumatakira/job_test_task">репозитории</a></li>
        <li>Заполните базу данными из главного меню - [News test]->[Database population (Faker)].</li></p>
        <li>Чтобы остановить приложение выполните из ранее открытого терминала: $docker-compose down. Для полного удаления приложения из вашей системы выполните дополнительно:
        <p>$ docker rmi rumatakira/job_test_task rumatakira/mysql-server</p>
        <p>$ docker volume rm jobtesttask_db_data</p>
        <li>И удалите саму директорию, которую вы создали в начале.</li>   
    </ul>
    <p><ul><b>Задания :</b>
        <li>Реализовать данную задачу с использованием фрэймворка Yii2</li>
        <li>Написать модуль редактирования и вывода новостей</li>
        <li>Модуль создания и редактирования новостей (админиский модуль):
        </br>Вывод всех новостей с сортировкой по дате(последние сверху). Создание и редактирование новостей У каждой новости есть поля: название, дата публикации, текст, тема(новость привязывается к одной теме из списка) Для редактирования поля дата использовать календарь jquery (http://jqueryui.com/)</li>
    </ul>
    </br><u>Модуль вывода новостей</u>
    </br>Слева вывод всех годов для которых есть публикации, для каждого года вывод месяцев в которых были публикации в скобках указывается количество публикаций за этот месяц, в виде:
    <ul>
        <li>2010</li>
        <li>сентябрь (1)</li>
        <li>июль (4)</li>
        <li>июнь (7)</li>
        <li>арт (12)</li>
        <li>февраль (3)</li>
        <li>2009</li>
    </ul>
    </br>При нажатии на год вывод всех новостей этого года, при нажатии на месяц вывод всех новостей этого месяца. Под годами вывод всех тем, в скобках количество публикаций по теме:
    <ul>
        <li>тема1 (2)</li>
        <li>тема2 (5)</li>
        <li>тема3 (7)</li>
    </ul>
    </br>При клике на название темы показываются все новости для которых выбрана эта тема. Справа вывод всех новостей в виде :
    <ul>
        <li>Название</li>
        <li>Дата публикации</li>
        <li>Тема</li>
        <li>Краткий текст(полный текст обрезанный 256 символов) ... читать далее(ссылка на новость)</li>
    </ul>
    </br>При клике на "читать далее" показывается новость в виде :
    <ul>
        <li>Название</li>
        <li>Дата публикации</li>
        <li>Тема</li>
        <li>Текст новости полностью</li>
        <li>все новости (ссылка на страницу с новостями)</li>
    </ul>
    </br>Вывод новостей по 5 на страницу, внизу страницы пейджер(вывод всех страниц, текущая страница подсвечивается). Гламурный дизайн не обязателен - главное реализовать данную задачу.</br>
    </br><u>SQL Задание</u>
    </br>Преобразовать данные таблицы таким образом, чтобы в ней содержались ТОЛЬКО транзакции-расходы. То есть, все транзакции-возвраты должны быть учтены в предшествующих им транзакциях-расходах. Пример. Дамп данных содержит две следующие транзакции.</br>
    <table style="width:100%">
        <tr>
            <th>ID</th>
            <th>Номер карты</th>
            <th>Дата/время</th>
            <th>Объем</th>
            <th>Услуга</th>
        </tr>
        <tr>
            <th>31769</th>
            <th>257473011</th>
            <th>2015-10-18 11:10:36</th>
            <th>-68</th>
            <th>ДТ</th>
        </tr>
        <tr>
            <th>31768</th>
            <th>257473011</th>
            <th>2015-10-18 11:21:15</th>
            <th>2.44</th>
            <th>ДТ</th>
        </tr>
    </table>
    </br>Где транзакция ID 31769 – расход, а ID 31768 возврат по этой транзакции. В результате преобразования, транзакция ID 31769 должна иметь объем -65.56, а транзакция ID 31768 должна быть удалена. Дамп .sql прилагается.
    </br><hr><a href="#TOP">GO TOP</a>
</div>
