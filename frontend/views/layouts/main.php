<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
Yii::setAlias('@CV_en', 'https://www.dropbox.com/s/i573ca8niv4l1xd/Kiryl-Lapchynski-cv-eng.pdf?dl=0');
Yii::setAlias('@CV_ru', 'https://www.dropbox.com/s/2lk6mczlsa5oifs/Kiryl-Lapchynski-cv-ru.pdf?dl=0');
Yii::setAlias('@repo', 'https://bitbucket.org/rumatakira/job_test_task');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Test tasks',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    $menuItems[] = ['label' => 'Task repo', 'url' => '@repo'];
    $menuItems[] = [
        'label' => 'My CV',
            'items' =>
            [
                [
                    'label' => 'CV in English',
                    'url' => '@CV_en',
                ],
                [
                    'label' => 'CV на русском',
                    'url' => '@CV_ru',
                ],
            ],
    ];
    $menuItems[] = [
        'label' => 'News test',
            'items' =>
            [
                [
                    'label' => 'Database population (Faker)',
                    'url' => ['/faker/faker'],
                ],
                [
                    'label' => 'Admin news module (test 1)',
                    'url' => ['/admin/news/index'],
                ],
                [
                    'label' => 'Show news (test 2)',
                    'url' => ['/NewsList/news-list/index'],
                ],
            ],
    ];
    $menuItems[] = [
        'label' => 'SQL test', 
        'url' => ['/SqlTest/sql-test/index'],
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <a href="https://bitbucket.org/rumatakira/">Kiryl Lapchynski</a> <?= date('Y') ?>.</p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
