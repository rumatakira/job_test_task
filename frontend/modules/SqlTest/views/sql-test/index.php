<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\SqlTest\models\SqlTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sql Test';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sql-test-form-index">
    <p>
        <?= Html::a('Find refinds', ['find-refund'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Recalculate', ['calculate-refund'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Roll back', ['undo-refund'], ['class' => 'btn btn-default']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'card_number',
            'date',
            'volume',
            'service',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}'],
        ],
    ]); ?>
    <p><?= Html::a('Reset search', ['index'], ['class' => 'btn btn-default']) ?></p>
</div>
