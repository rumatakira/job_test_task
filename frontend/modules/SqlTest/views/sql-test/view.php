<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\SqlTest\models\SqlTestForm */

$this->title = 'Record nomber :'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sql test', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sql-test-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'card_number',
            'date',
            'volume',
            'service',
            'address_id',
        ],
    ]) ?>
    
    <p><?= Html::a('All records', ['/SqlTest/sql-test/index'], ['class' => 'btn btn-default']) ?></p>

</div>
