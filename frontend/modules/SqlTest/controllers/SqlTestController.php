<?php

namespace app\modules\SqlTest\controllers;

use Yii;
use app\modules\SqlTest\models\SqlTestForm;
use app\modules\SqlTest\models\SqlTestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SqlTestController implements the CRUD actions for SqlTestForm model.
 */
class SqlTestController extends Controller
{

    /**
     * Lists all SqlTestForm models.
     * @return mixed
     */
    public function actionIndex($query = null)
    {
        $searchModel = new SqlTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Find refunds in database.
     */
    public function actionFindRefund()
    {
        $query = SqlTestForm::find()
                ->where(['SIGN(volume)' => 1]);
           
        return $this->actionIndex($query);
    }

    /**
     * Calculate refunds and change database
     * 
     */
    public function actionCalculateRefund()
    {
        Yii::$app->db->createCommand(' 
            CREATE PROCEDURE cr()
            BEGIN
                SELECT COUNT(*) INTO @nrow 
                    FROM data WHERE SIGN(volume)=1;
                label1: WHILE @nrow > 0 DO
                    SELECT volume, id INTO @xvolume, @xid
                        FROM data WHERE SIGN(volume)=1 LIMIT 1;
                    SELECT volume, id INTO @yvolume, @yid
                        FROM data WHERE id = @xid+1 LIMIT 1;
                    UPDATE data SET volume = @xvolume + @yvolume WHERE id = @yid;
                    DELETE FROM data WHERE id = @xid;
                    SET @nrow := @nrow -1;
                END WHILE label1;
            END;
            CALL cr();
            DROP PROCEDURE cr;
        ')->execute();

        return $this->redirect(['index']);
    }

    /**
     * Drop table {{data}}. Execute sql dump file.
     * 
     * @return redirect
     */
    public function actionUndoRefund()
    {
        set_time_limit(600);
        Yii::$app->db->createCommand()->dropTable('{{data}}')->execute();
        $filename = __DIR__.'/../dump_test_sql.sql';
        $lines = file_get_contents($filename);
        Yii::$app->db->createCommand($lines)->execute();

        return $this->redirect(['index']);
    }
    
    /**
     * Displays a single SqlTestForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
     /**
     * Finds the SqlTestForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SqlTestForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SqlTestForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
