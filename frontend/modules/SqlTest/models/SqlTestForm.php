<?php

namespace app\modules\SqlTest\models;

/**
 * This is the model class for table "data".
 *
 * @property integer $id
 * @property string $card_number
 * @property string $date
 * @property double $volume
 * @property string $service
 * @property integer $address_id
 */
class SqlTestForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_number' => 'Card Number',
            'date' => 'Date',
            'volume' => 'Volume',
            'service' => 'Service',
            'address_id' => 'Address ID',
        ];
    }
}
