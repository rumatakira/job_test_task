<?php

namespace app\modules\SqlTest\models;

use yii\data\ActiveDataProvider;
use app\modules\SqlTest\models\SqlTestForm;

/**
 * SqlTestSearch represents the model behind the search form about `app\modules\SqlTest\models\SqlTestForm`.
 */
class SqlTestSearch extends SqlTestForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'address_id','card_number', 'date', 'service', 'volume'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $query)
    {
        If (!$query) {
            $query = SqlTestForm::find();
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 7],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'volume' => $this->volume,
            'address_id' => $this->address_id,
        ]);

        $query->andFilterWhere(['like', 'card_number', $this->card_number])
            ->andFilterWhere(['like', 'service', $this->service]);

        return $dataProvider;
    }
    
}
