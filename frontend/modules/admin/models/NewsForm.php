<?php

namespace app\modules\admin\models;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $news_title
 * @property string $news_created_at
 * @property string $news_body
 * @property string $news_topic
 */
class NewsForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_title', 'news_created_at', 'news_body', 'news_topic'], 'required'],
            [['news_title', 'news_body', 'news_topic'], 'trim'],
            ['news_created_at', 'date', 'format' => 'yyyy-MM-dd'],
            [['news_body'], 'string', 'min' => 5],
            [['news_title', 'news_topic'], 'string', 'min' => 5, 'max' => 255],
        ];
    }

    /**
     * Create new news.
     *
     * @return news|null the saved model or null if saving fails
     */
    public function createNews()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $news = new news();
        $news->news_title = $this->news_title;
        $news->news_created_at = $this->news_created_at;
        $news->news_body = $this->news_body;
        $news->news_topic = $this->news_topic;
        
        return $news->save() ? $news : null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'News id',
            'news_title'        => 'News title',
            'news_created_at'   => 'News date',
            'news_body'         => 'News body',
            'news_topic'        => 'News topic',            
        ];
    }
}
