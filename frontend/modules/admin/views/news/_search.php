<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12" align=justify>
    <div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'news_title') ?>

    <?= $form->field($model, 'news_created_at')->hint('You can enter in format YYYY-MM-DD, or choose from widget:')->widget(DatePicker::classname(), [
            'dateFormat' => 'yyyy-MM-dd', 'clientOptions' => ['changeMonth' => 'true', 'changeYear' => 'true', 'autoSize' => 'true']])->textInput() ?>

    <?= $form->field($model, 'news_body') ?>

    <?= $form->field($model, 'news_topic') ?>

    <div class="form-group">
        <?= Html::submitButton('Find', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', ['/admin/news/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>
