<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsForm */

$this->title = 'Edit news number: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin news module (test 1)', 'url' => ['/admin/news/index']];
$this->params['breadcrumbs'][] = ['label' => 'News number: '.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Edit news';
?>
<div class="col-md-12" align=justify>
    <div class="row">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    </div>
</div>
