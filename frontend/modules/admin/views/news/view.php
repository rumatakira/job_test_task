<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsForm */

$this->title = 'News number: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin news module (test 1)', 'url' => ['/admin/news/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12" align=justify>
    <div class="row">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'news_title',
            'news_created_at',
            'news_body:ntext',
            'news_topic',
        ],
    ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    </div>
</div>
