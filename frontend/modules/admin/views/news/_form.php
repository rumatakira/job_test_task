<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12" align=justify>
    <div class="row">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'news_title')->textInput(['maxlenth' => true]) ?>

    <?= $form->field($model, 'news_created_at')->hint('You can enter in format YYYY-MM-DD, or choose from widget:')->widget(DatePicker::classname(), [
            'dateFormat' => 'yyyy-MM-dd', 'clientOptions' => ['changeMonth' => 'true', 'changeYear' => 'true', 'autoSize' => 'true']])->textInput() ?>

    <?= $form->field($model, 'news_body')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'news_topic')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Edit', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['/admin/news/index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>
</div>
