<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin news module (test 1)';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-12" align=justify>
    <div class="row">

        <h1><?= Html::encode($this->title) ?></h1>

        <p><?= Html::a('Create news', ['create'], ['class' => 'btn btn-success']) ?></p>

        </br>
        <p>Search: You can search through the form below or through the appropriate columns in the fields of the table, combining them and pressing ENTER. To reset, please use the "Reset" button.</p>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </br>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'id',
                    'contentOptions' => ['style' => 'text-align:center']
                ],
                'news_title:ntext',
                [
                    'attribute' => 'news_created_at',
                    'contentOptions' => ['style' => 'text-align:center; min-width:120px'],
                ],
                'news_body:ntext',
                [
                    'attribute' => 'news_topic',
                    'contentOptions' => ['style' => 'text-align:center'],
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
    </div>
</div>
