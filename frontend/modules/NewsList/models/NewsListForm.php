<?php

namespace app\modules\NewsList\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $news_title
 * @property string $news_created_at
 * @property string $news_body
 * @property string $news_topic
 */
class NewsListForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'News id',
            'news_title'        => 'News title',
            'news_created_at'   => 'News date',
            'news_body'         => 'News body',
            'news_topic'        => 'News topic',            
        ];
    }
}
