<?php

namespace app\modules\NewsList\models;

use yii\data\ActiveDataProvider;
use app\modules\NewsList\models\NewsListForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * NewsListSearch represents the model behind the search form about `app\modules\NewsList\models\NewsListForm`.
 */
class NewsListSearch extends NewsListForm
{

    /**
     * Creates data for the right view side - provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchRighSide($params, $year, $month, $topic)
    {
        /* uncomment for russian month
         * if ($month !== null) {
            $this->monthToInt($month);
        }*/
        
        if (!$year and !$topic) {
            
            // search for the basic index.php
            
            $queryRightSide = NewsListForm::find();
            
        } elseif ($year !== null and !$month and !$topic) {
            
            // seach by year
            
            $queryRightSide = NewsListForm::find()
                ->where(['YEAR(news_created_at)' => $year])
                ->orderBy(['news_created_at' => SORT_DESC]);
            
        } elseif ($year !== null and $month !== null and !$topic) {
            
            // seach by month
            
            $queryRightSide = NewsListForm::find()
                // uncomment for russian month
                //->where(['YEAR(news_created_at)' => $year, 'MONTH(news_created_at)' => $month])
                
                //comment if russian month     
                ->where(['YEAR(news_created_at)' => $year, 'MONTHNAME(news_created_at)' => $month])
                ->orderBy(['news_created_at' => SORT_DESC]);
            
         
        } elseif (!$year and !$month and $topic !== null) {
            
            //search by topic
            
            $queryRightSide = NewsListForm::find()
                ->where(['news_topic' => $topic]);
        }
        
        // add conditions that should always apply here

        $dataProviderRightSide = new ActiveDataProvider([
            'query' => $queryRightSide,
            'pagination' => ['pageSize' => 5],
            'sort'       => [
                'attributes'   => [
                    'news_created_at',
                ],
                'defaultOrder' => ['news_created_at' => SORT_DESC],
            ],
        ]);

        $this->load($params);
        
        return $dataProviderRightSide;
    }
    
    /**
     * Creates query array for the view in the left side
     *
     * @param array $params
     *
     * @return array
     */
    public function searchLeftSide($params)
    {
        //uncomment for russian month
        //$queryLeftSide = Yii::$app->db->createCommand('SELECT YEAR(news_created_at) AS YEAR, MONTH(news_created_at) AS MONTH, [[news_topic]] FROM {{news}} ORDER BY news_created_at DESC')->queryAll();
        
        //comment if russian month
        $queryLeftSide = Yii::$app->db->createCommand('SELECT YEAR(news_created_at) AS YEAR, MONTHNAME(news_created_at) AS MONTH, [[news_topic]] FROM {{news}} ORDER BY news_created_at DESC')->queryAll();
        
        $this->load($params);
        
        return $queryLeftSide;
    }
    
    /**
     * Change month numbers to the russian in the query array for the left side.
     * Prepare array of dates and array of topics for the view in the left side.
     * Unset base query array.
     * 
     * @param reference $array
     * 
     * @return array
     */
    public function queryArrayPrepare(&$array) 
    {
        $array = ArrayHelper::index($array, null, 'YEAR');
        foreach ($array as $key => &$value) {
            foreach ($value as $key2 => &$value2) {
                /*uncomment for russian month
                 * if ($value[$key2]['MONTH'] == '1') {
                    $value[$key2]['MONTH'] = 'январь';
                } elseif ($value[$key2]['MONTH'] == '2') {
                    $value[$key2]['MONTH'] = 'февраль';
                } elseif ($value[$key2]['MONTH'] == '3') {
                    $value[$key2]['MONTH'] = 'март';
                } elseif ($value[$key2]['MONTH'] == '4') {
                    $value[$key2]['MONTH'] = 'апрель';
                } elseif ($value[$key2]['MONTH'] == '5') {
                    $value[$key2]['MONTH'] = 'май';
                } elseif ($value[$key2]['MONTH'] == '6') {
                    $value[$key2]['MONTH'] = 'июнь';
                } elseif ($value[$key2]['MONTH'] == '7') {
                    $value[$key2]['MONTH'] = 'июль';
                } elseif ($value[$key2]['MONTH'] == '8') {
                    $value[$key2]['MONTH'] = 'август';
                } elseif ($value[$key2]['MONTH'] == '9') {
                    $value[$key2]['MONTH'] = 'сентябрь';
                } elseif ($value[$key2]['MONTH'] == '10') {
                    $value[$key2]['MONTH'] = 'октябрь';
                } elseif ($value[$key2]['MONTH'] == '11') {
                    $value[$key2]['MONTH'] = 'ноябрь';
                } elseif ($value[$key2]['MONTH'] == '12') {
                    $value[$key2]['MONTH'] = 'декабрь';
                }*/
                unset($value2['YEAR']);
                $dateArray[$key][] = $value[$key2]['MONTH'];
                $topics[] = $value[$key2]['news_topic'];
            }
        }
       
        unset($array);
        
        return ['dateArray' => $dateArray, 'topics' => $topics];
    }
    
      /**
     * Change russian month to numbers
     * 
     * @param reference $month
     * 
     */
    public function monthToInt(&$month) 
    {
       switch ($month) {
            case 'январь':
                $month = 1;
                break;
            case 'февраль':
                $month = 2;
                break;
            case 'март':
                $month = 3;
                break;
            case 'апрель':
                $month = 4;
                break;
            case 'май':
                $month = 5;
                break;
            case 'июнь':
                $month = 6;
                break;
            case 'июль':
                $month = 7;
                break;
            case 'август':
                $month = 8;
                break;
            case 'сентябрь':
                $month = 9;
                break;
            case 'октябрь':
                $month = 10;
                break;
            case 'ноябрь':
                $month = 11;
                break;
            case 'декабрь':
                $month = 12;
                break;
       }
    }
            
}