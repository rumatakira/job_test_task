<?php

namespace app\modules\NewsList\controllers;

use Yii;
use app\modules\NewsList\models\NewsListForm;
use app\modules\NewsList\models\NewsListSearch;
use yii\web\Controller;

/**
 * NewsListController implements the LIST actions for NewsListForm model.
 */
class NewsListController extends Controller
{

    /**
     * Lists all NewsListForm models.
     * @return mixed
     */
    public function actionIndex($year = null, $month = null, $topic = null)
    {
        $searchModel = new NewsListSearch();
        $dataProviderRightSide = $searchModel->searchRighSide(Yii::$app->request->queryParams, $year, $month, $topic);
        $queryLeftSide = $searchModel->searchLeftSide(Yii::$app->request->queryParams);
        $leftArray = $searchModel->queryArrayPrepare($queryLeftSide);
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProviderRightSide' => $dataProviderRightSide,
            'dateArray' => $leftArray['dateArray'],
            'topics' => $leftArray['topics'],
        ]);
    }
    
    /**
     * Displays a single NewsForm model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
       /**
     * Finds the NewsForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NewsForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NewsListForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
}
