<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\NewsList\models\NewsListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Вывод новостей (test 2)';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1></br>

<div class="col-md-4" align=left>
    <div class="row">
        <?= \yii\helpers\VarDumper::dump($dateArray); ?>
        
    </div>
</div>

<div class="col-md-8" align=justify>
    <div class="row">

        <?= ListView::widget([
            'dataProvider' => $dataProviderRightSide,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_item',['model' => $model]);
            },
                
        ]) ?>

    </div>
</div>
        

