<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>

</br>
<p><b>News name: </b><?= Html::encode($model->news_title); ?></p>
<p><b>News date: </b><?= $model->news_created_at; ?></p>
<p><b>News topic: </b><?= Html::encode($model->news_topic); ?></p>
<p><b>News body: </b>

    <?php 
    $lessText = substr($model->news_body, 0, 255);
    echo HtmlPurifier::process($lessText).'...'.Html::a('Reed more', ['view', 'id' => $model->id]);
    ?>
    
</p><hr>
