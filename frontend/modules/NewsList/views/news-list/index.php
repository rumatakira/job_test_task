<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\NewsList\models\NewsListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Show news (test 2)';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1></br>
<p>
    <?= Html::a('All news', ['index'], ['class' => 'btn btn-default']) ?>
</p>
<hr>

<div class="col-md-4" align=left>
    <div class="row">
        <p><u><b>By dates :</b></u></p>
        <?php foreach ($dateArray as $key => $value): ?>
        
            <b><?= Html::a($key, ['index', 'year' => $key]) ?></b>

            <?php $countD = array_count_values($value); ?>

            <?php foreach ($countD as $key2 => $value2): ?>

                <p> &nbsp;&nbsp;&nbsp;<?= Html::a($key2,['index', 'year' => $key, 'month' => $key2]).' ('.$value2.')'; ?><p>

            <?php endforeach; ?>
        
        <?php endforeach; ?>
                    
        </br><p><u><b>By topics :</b></u></p>
        
        <?php $countT = array_count_values($topics); ?>
        
        <?php foreach ($countT as $key => $value): ?>

            <p> <?= Html::a($key, ['index', 'topic' => $key]).' ('.$value.')'; ?><p>

        <?php endforeach; ?>
        
    </div>
</div>

<div class="col-md-8" align=justify>
    <div class="row">

        <?= ListView::widget([
            'dataProvider' => $dataProviderRightSide,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_item',['model' => $model]);
            },
                
        ]) ?>

    </div>
</div>
        

