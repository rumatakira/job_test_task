<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NewsForm */

$this->title = 'News number: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Show news (test 2)', 'url' => ['/NewsList/news-list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-12" align=justify>
    <div class="row">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'news_title',
            'news_created_at',
            'news_body:ntext',
            'news_topic',
        ],
    ]) ?>

    <p><?= Html::a('All news', ['/NewsList/news-list/index'], ['class' => 'btn btn-default']) ?></p>
    
    </div>
</div>
