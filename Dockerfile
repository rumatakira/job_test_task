FROM ubuntu:16.04

LABEL vendor="Kiryl A.Lapchynski"
LABEL vendor.email="rumatakira74@gmail.com"
LABEL project="job_test_task hhvm-proxygen + Yii2 + mysql based"
LABEL progect.repo="https://github.com/rumatakira/job_test_task"

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

#set locales
RUN apt-get update && apt-get install -y \
  apt-utils \
  locales \
  && rm -rf /var/lib/apt/lists/* \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

#HHVM install
RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449
RUN apt-get update && apt-get install -y software-properties-common \
  && add-apt-repository "deb http://dl.hhvm.com/ubuntu xenial-lts-3.18 main" \
  && apt-get update \
  && apt-get install -y hhvm 

RUN apt-get clean \
 && rm -rf /var/lib/apt/lists/*

COPY . /app
COPY hhvm-conf /etc/hhvm/

RUN hhvm-repo-mode enable /app

EXPOSE 80

WORKDIR /app

CMD ["/usr/bin/hhvm", "-m", "server", "-c", "/etc/hhvm/server.ini"]
