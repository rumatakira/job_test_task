<?hh

use yii\db\Migration;

class m160913_020226_data_table_create_and_populate extends Migration
{
    public function up()
    {
        $this->execute(file_get_contents(__DIR__.'/../../frontend/modules/SqlTest/dump_test_sql.sql'));
    }

    public function down()
    {
         $this->dropTable('{{%data}}');
    }

}
