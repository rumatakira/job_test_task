<?hh

use yii\db\Migration;

class m160825_184009_news extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'news_title' => $this->string()->notNull(),
            'news_created_at' => $this->date()->notNull(),
            'news_body' => $this->text()->notNull(),
            'news_topic' => $this->string()->notNull(),
        ], $tableOptions);
        
        $this->createIndex('news_created_at', '{{%news}}', 'news_created_at');
    }

    public function down()
    {
        $this->dropTable('{{%news}}');
    }
}