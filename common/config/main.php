<?hh
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'NewsList' => [
            'class' => 'app\modules\NewsList\Module',
        ],
        'SqlTest' => [
            'class' => 'app\modules\SqlTest\Module',
        ],
    ],
    'language' => 'en_EN',
];
